
def fib2(n):
    """Return a list containing the Fibonacci series up to n."""

    result = []
    a, b = 0, 1
    while a < n:
        result.append(a)    # see below
        a, b = b, a+b
    return result

if __name__ == '__main__':
    f100 = fib2(100)    # call it
    print f100                # write the result
